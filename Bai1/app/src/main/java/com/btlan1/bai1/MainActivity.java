package com.btlan1.bai1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final SmsManager smsManager = SmsManager.getDefault();
        Button btnSubmit = (Button) findViewById(R.id.btn_submit);
        final RadioGroup rgSize = (RadioGroup) findViewById(R.id.rg_size);
        final RadioGroup rgTortilla = (RadioGroup) findViewById(R.id.rg_tortilla);
        final CheckBox cbBeef = (CheckBox) findViewById(R.id.cb_beef);
        final CheckBox cbChicken = (CheckBox) findViewById(R.id.cb_chicken);
        final CheckBox cbRice = (CheckBox) findViewById(R.id.cb_rice);
        final CheckBox cbCheese = (CheckBox) findViewById(R.id.cb_cheese);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton rbSize = (RadioButton) findViewById(rgSize.getCheckedRadioButtonId());
                RadioButton rbTortilla = (RadioButton) findViewById(rgTortilla.getCheckedRadioButtonId());

                String message = "I want a " + rbSize.getText() + " Taco, with " +
                        rbTortilla.getText() + " Tortilla. Fillings: " +
                        (!cbBeef.isChecked() ? "" : " +" + cbBeef.getText()) +
                        (!cbChicken.isChecked() ? "" : " +" + cbChicken.getText()) +
                        (!cbRice.isChecked() ? "" : " +" + cbRice.getText()) +
                        (!cbCheese.isChecked() ? "" : " +" + cbCheese.getText()) + ".";
                String phoneNumber = "5556";
                Intent intent = new Intent(Intent.ACTION_SENDTO,
                        Uri.parse("sms:" + phoneNumber));
                intent.putExtra("sms_body", message);
                startActivity(intent);
            }
        });
    }
}
