package com.btlan1.bai2;

public class Product {
    public int id;
    public int img;
    public String name;
    public int highResImg;
    public String detailUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product (int id, int img, String name, int highResImg, String detailUrl) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.highResImg = highResImg;
        this.detailUrl = detailUrl;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public int getHighResImg() {
        return highResImg;
    }

    public void setHighResImg(int highResImg) {
        this.highResImg = highResImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
