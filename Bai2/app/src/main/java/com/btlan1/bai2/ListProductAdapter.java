package com.btlan1.bai2;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListProductAdapter extends BaseAdapter {
    private ArrayList<Product> products;
    private Activity activity;

    public ListProductAdapter(ArrayList<Product> products, Activity activity) {
        this.products = products;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        convertView = inflater.inflate(R.layout.product_item, parent, false);

        ImageView ivProduct = (ImageView) convertView.findViewById(R.id.iv_product);
        TextView tvProduct = (TextView) convertView.findViewById(R.id.tv_product);
        Button btn = (Button) convertView.findViewById(R.id.btn_high_res_picture);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parent.getContext(), ProductImageActivity.class);
                intent.putExtra("imgResId", products.get(position).getHighResImg());
                parent.getContext().startActivity(intent);
            }
        });
        ivProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parent.getContext(), ProductDetailActivity.class);
                intent.putExtra("url", products.get(position).getDetailUrl());
                parent.getContext().startActivity(intent);
            }
        });
        ivProduct.setImageResource(products.get(position).getImg());
        tvProduct.setText(products.get(position).getName());

        return convertView;
    }
}
