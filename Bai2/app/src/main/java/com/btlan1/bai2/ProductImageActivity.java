package com.btlan1.bai2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class ProductImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_image);

        Bundle extras = getIntent().getExtras();

        ImageView imageView = (ImageView) findViewById(R.id.iv_high_res_image);
        imageView.setImageResource(extras.getInt("imgResId"));
    }
}
