package com.btlan1.bai2;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(1, R.drawable.sup_tom_photo,
                "Sup Tom",
                R.drawable.sup_tom_hdphoto,
                "https://wikiohana.net/am-thuc/nau-canh/sup-tom/"));
        products.add(new Product(2, R.drawable.sup_khoai_tay_photo,
                "Sup Khoai Tay",
                R.drawable.sup_khoai_tay_hdphoto,
                "https://cookpad.com/vn/cong-thuc/12240474-sup-khoai-tay%E2%99%A5%EF%B8%8F?via=search&search_term=s%C3%BAp%20khoai%20t%C3%A2y"));
        ListView listView = (ListView) findViewById(R.id.lv_list_product);

        ListProductAdapter adapter = new ListProductAdapter(products, this);

        listView.setAdapter(adapter);
    }
}
