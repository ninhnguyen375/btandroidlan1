package com.btlan1.bai4;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class HistoryListAdapter extends BaseAdapter {
    ArrayList<String> history;
    Activity activity;

    public HistoryListAdapter(ArrayList<String> history, Activity activity) {
        this.history = history;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return history.size();
    }

    @Override
    public Object getItem(int position) {
        return history.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        convertView = inflater.inflate(R.layout.tv_item_list, parent, false);
        TextView tv = convertView.findViewById(R.id.tv_history_item);
        tv.setText(history.get(position));

        return convertView;
    }
}
