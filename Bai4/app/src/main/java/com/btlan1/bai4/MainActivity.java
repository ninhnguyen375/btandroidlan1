package com.btlan1.bai4;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public ArrayList<String> history = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null) {
            TextView tvResult = (TextView) findViewById(R.id.tv_result);
            ListView lvHistory = (ListView) findViewById(R.id.lv_history);

            history = savedInstanceState.getStringArrayList("HISTORY");
            HistoryListAdapter adapter = new HistoryListAdapter(history, MainActivity.this);

            tvResult.setText(savedInstanceState.getString("RESULT"));
            lvHistory.setAdapter(adapter);
        }

        Button btnSubmit = (Button) findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup rgSelect = (RadioGroup) findViewById(R.id.rg_select);
                EditText etValue = findViewById(R.id.et_value);
                String value = etValue.getText().toString();
                TextView tvResult = (TextView) findViewById(R.id.tv_result);
                ListView lvHistory = (ListView) findViewById(R.id.lv_history);
                String historyItem = "";

                double result;

                if (value.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Please input value to convert", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    Double.parseDouble(value);
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Please input a number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (rgSelect.getCheckedRadioButtonId() == R.id.rb_ftc) {
                    result = (Double.parseDouble(value) - 32.0) * 5.0 / 9.0;
                    result = (double) Math.round(result * 100) / 100;
                    historyItem = "F to C: " + value + " => " + String.valueOf(result);
                } else {
                    result = (Double.parseDouble(value) * 9.0 / 5.0) + 32.0;
                    result = (double) Math.round(result * 100) / 100;
                    historyItem = "C to F: " + value + " => " + String.valueOf(result);
                }

                tvResult.setText(String.valueOf(result));
                history.add(historyItem);

                HistoryListAdapter adapter = new HistoryListAdapter(history, MainActivity.this);
                lvHistory.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView tvResult = (TextView) findViewById(R.id.tv_result);
        if(!String.valueOf(tvResult.getText()).isEmpty()) {
            outState.putString("RESULT", String.valueOf(tvResult.getText()));
            outState.putStringArrayList("HISTORY", history);
        }
    }
}
